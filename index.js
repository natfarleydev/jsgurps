// A holder for making json objects
var json = {
    create_meta: (name, value) => ({
        name: name,
        value: value
    }),
    create_basic_attribute: (name, points, level) => ({
        name: name,
        points: points,
        level: level,
        type: "basic_attribute"
    }),
    create_secondary_characteristic: (name, points, level, based_on) => ({
        name: name,
        points: points,
        level: level,
        based_on: based_on,
        type: "secondary_characteristic"
    }),
    create_advantage: (name, points, level) => ({
        name: name,
        points: points,
        level: level,
        type: "advantage"
    }),
    create_disadvantage: (name, points, level) => ({
        name: name,
        points: points,
        level: level,
        type: "disadvantage"
    }),
    create_perk: name => ({
        name: name,
        points: 1,
        type: "perk"
    }),
    create_quirk: name => ({
        name: name,
        points: -1,
        type: "quirk"
    }),
    create_skill: (name, points, level, based_on) => ({
        name: name,
        points: points,
        level: level,
        based_on: based_on,
        type: "skill"
    }),
    create_spell: (name, points, level, based_on) => ({
        name: name,
        points: points,
        level: level,
        based_on: based_on,
        type: "spell"
    }),
};